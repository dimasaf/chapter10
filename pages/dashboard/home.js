import React, { Fragment } from "react";
import Navbar from "../../components/navbar";
import Dashboard from "../../components/home/Home";

function Home() {
  return (
    <Fragment>
      <Navbar />
      <Dashboard />
    </Fragment>
  );
}

export default Home;
