import React, { Fragment } from "react";
import Navbar from "../../components/navbar";
import LeaderboardComponent from "../../components/leaderboard/LeaderboardComponent";

function Leaderboard() {
  return (
    <Fragment>
      <Navbar />
      <LeaderboardComponent />
    </Fragment>
  );
}

export default Leaderboard;
