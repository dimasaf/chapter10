import React, { Fragment } from "react";
import Navbar from "../../components/Navbar";
import ProfilePage from "../../components/profile/Profile";

function Profile() {
  return (
    <Fragment>
      <Navbar />
      <ProfilePage />
    </Fragment>
  );
}

export default Profile;
