import React, { Fragment } from "react";
import Navbar from "../components/profileUpdate/Navbar";
import ProfileUpdate from "../components/profileUpdate/ProfileUpdate";

function ProfileUpdatePage () {
  return (
    <Fragment>
      <Navbar/>
      <ProfileUpdate/>
    </Fragment>
  )
};

export default ProfileUpdatePage;
