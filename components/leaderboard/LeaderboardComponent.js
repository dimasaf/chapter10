import classes from "./Leaderboard.module.css";
import Table from "../table/TableComponent";
import Link from "next/link";

const LeaderboardComponent = () => {
  return (
    <section className={classes.starting}>
      <div className={classes.Nav}>
        <h4>Leaderboard!</h4>
      </div>
      <hr />
      <Table />
      <a href="/dashboard/game" className={classes.Button}>
        Let's Play
      </a>
    </section>
  );
};

export default LeaderboardComponent;
