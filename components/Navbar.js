import React, { Fragment } from "react";
import Link from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { logout } from "../stores/authSlice";
import { useRouter } from "next/router";

function Navbar() {
  const auth = useSelector((state) => state.auth);
  const router = useRouter();

  const dispatch = useDispatch();

  const handleLogOut = (e) => {
    e.preventDefault();
    router.push("/");
    dispatch(logout());
  };

  return (
    <nav className="px-10 py-6 bg-slate-600 flex justify-between align-middle">
      <Link href="/">
        <a className="text-white font-bold text-4xl">HOME</a>
      </Link>
      <ul className="flex items-center text-white font-semibold">
        {auth.isLoggedIn ? (
          <Fragment>
            <Link href="/dashboard/home">
              <li className="mx-4 cursor-pointer">{`Hai ${auth.email}`}</li>
            </Link>
            <Link href="/dashboard/profile">
              <li className="mx-4 cursor-pointer">Profile</li>
            </Link>
            <Link href="/dashboard/leaderboard">
              <li className="mx-4 cursor-pointer">leaderboard</li>
            </Link>
            <button onClick={handleLogOut}>
              <li className="mx-4 cursor-pointer">Log out</li>
            </button>
          </Fragment>
        ) : (
          <Fragment>
            <Link href="/login">
              <li className="mx-4 cursor-pointer">Login</li>
            </Link>
            <Link href="/register">
              <li className="mx-4 cursor-pointer">Register</li>
            </Link>
          </Fragment>
        )}
      </ul>
    </nav>
  );
}

export default Navbar;
