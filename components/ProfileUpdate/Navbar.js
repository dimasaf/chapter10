import React from "react";
import Link from "next/link";

function Navbar() {
  return (
    <nav className="px-10 py-6 bg-slate-600 flex justify-between align-middle">
      <Link href="/">
        <a className="text-white font-bold text-4xl">Profile Update</a>
      </Link>
      <ul className="flex items-center text-white font-semibold">
        <Link href="/home">
        Home</Link>
        <Link href="/profile">
          <li className="mx-4 cursor-pointer">Profile</li>
        </Link>
        <Link href="/logout">
          <li className="mx-4 cursor-pointer">Logout</li>
        </Link>
      </ul>
    </nav>
  );
}

export default Navbar;
