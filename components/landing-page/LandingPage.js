import classes from './LandingPage.module.css';

const LandingPage = () => {
  return (
    <section className={classes.starting}>
      <h1>Welcome Gamers!</h1>
      <p>Welcome to game Rock-Paper-Scissor.</p>
      <hr/>
    </section>
  );
};

export default LandingPage;
