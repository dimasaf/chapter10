import React, { Fragment } from "react";
import LandingPageComponent from "./landing-page/LandingPage";
import Navbar from "./navbar";

function LandingPage() {
  return (
    <Fragment>
      <Navbar />
      <LandingPageComponent />
    </Fragment>
  );
}

export default LandingPage;
