import { createUserWithEmailAndPassword, getAuth } from "firebase/auth";
import { collection, getDocs } from "firebase/firestore";
import { useEffect, useState } from "react";
import { db } from "../../utils/firebase-config";
import Image from "next/image";
import RoomImage from "../../assets/images/room-image.jpg";
import classes from "./Home.module.css";
import Link from "next/link";

function Dashboard() {
  // List batch of users, 1000 at a time.
  const [allUser, setAllUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  function getUsers() {
    const user = collection(db, "users");
    getDocs(user)
      .then((response) => {
        const user = response.docs.map((doc) => ({
          data: doc.data(),
          id: doc.id,
        }));
        setAllUsers(user);
      })
      .catch((err) => console.warn(err.message));
  }

  return (
    <div>
      <div className={classes.row}>
        <div className={classes.card}>
          <a href="#" className={classes.a}>
            <Image src={RoomImage} width="300" className={classes.cardImage} />
          </a>
          <div className={classes.cardBody}>
            <p className={classes.cardTittle}>Gunting Batu Kertas</p>
            <p className={classes.cardDesc}>
              Bermain dan dapatkan poin saat menang
            </p>
            <Link href="/dashboard/game">
              <a href="" className={classes.cardButton}>
                Join
              </a>
            </Link>
          </div>
        </div>
        <div className={classes.card}>
          <a href="#" className={classes.a}>
            <Image src={RoomImage} width="300" className={classes.cardImage} />
          </a>
          <div className={classes.cardBody}>
            <p className={classes.cardTittle}>comming soon ...</p>
            <p className={classes.cardDesc}>game lainnya akan hadir</p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
