import classes from "./TableComponent.module.css";

const Table = () => {
  return (
    <table className={classes.Table}>
      <tr>
        <th>Rank</th>
        <th>Player</th>
        <th>Score</th>
      </tr>
      <tr>
        <td>#1</td>
        <td>Player 1</td>
        <td>5040</td>
      </tr>
      <tr>
        <td>#2</td>
        <td>Player 2</td>
        <td>1234</td>
      </tr>
      <tr>
        <td>#3</td>
        <td>Player 3</td>
        <td>1000</td>
      </tr>
      <tr>
        <td>#4</td>
        <td>Player 4</td>
        <td>158</td>
      </tr>
      <tr>
        <td>#5</td>
        <td>Player 5</td>
        <td>52</td>
      </tr>
    </table>
  );
};

export default Table;
