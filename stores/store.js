import { configureStore } from "@reduxjs/toolkit";
import exampleSlice from "./exampleSlice";
import authSlice from "./authSlice";

export const store = configureStore({
  reducer: {
    auth: authSlice,
  },
});
