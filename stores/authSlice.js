import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  token: "",
  isLoggedIn: false,
  email: "",
  isLoading: false,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload;
      localStorage.setItem("token", action.payload);
    },
    setIsLoggedIn: (state, action) => {
      state.isLoggedIn = action.payload;
    },
    setEmail: (state, action) => {
      state.email = action.payload;
    },
    logout: (state) => {
      state.token = "";
      state.isLoggedIn = false;
      state.email = "";
      state.isLoading = false;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setToken, setIsLoggedIn, logout, setEmail } = authSlice.actions;

export default authSlice.reducer;
